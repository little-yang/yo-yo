import QQMapWX from '@/common/js/qqmap-wx-jssdk.js';
const uploadUrl = ''; // 上传地址
const devConfig = {
    baseUrl: 'https://yo.docshare.org.cn',
    staticUrl: 'https://yo.docshare.org.cn', // 静态资源地址
    uploadUrl: uploadUrl, // 上传地址
};

const prodConfig = {
    baseUrl: 'https://yo.docshare.org.cn',
    staticUrl: 'https://yo.docshare.org.cn', // 静态资源地址
    uploadUrl: uploadUrl, // 上传地址
};

let config;
if (process.env.NODE_ENV === 'development' || !process.env.NODE_ENV) {
    config = devConfig;
} else if (process.env.NODE_ENV === 'production') {
    config = prodConfig;
}

export default config;