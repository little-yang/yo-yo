import App from './App'
// #ifndef VUE3
import Vue from 'vue'
import '@/common/css/uni.css';
import "@/common/css/amazeui.css";
import "@/common/css/style.css";
Vue.config.productionTip = true	
App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif